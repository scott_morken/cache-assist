<?php

namespace Smorken\CacheAssist;

use Illuminate\Cache\CacheManager;
use Smorken\CacheAssist\Contracts\CacheOptions;

class CacheAssist implements \Smorken\CacheAssist\Contracts\CacheAssist
{
    public static int $maxLength = 0;

    protected static CacheManager $cache;

    protected CacheOptions $cacheOptions;

    public function __construct(CacheOptions $cacheOptions)
    {
        $this->setCacheOptions($cacheOptions);
    }

    public static function mock(): void
    {
        $cache = \Mockery::mock(CacheManager::class);
        // @phpstan-ignore method.notFound
        $cache->shouldReceive('remember')->andReturnUsing(
            function (...$args) {
                return $args[2]();
            }
        );
        // @phpstan-ignore argument.type
        self::setCache($cache);
    }

    public static function setCache(CacheManager $cache): void
    {
        self::$cache = $cache;
    }

    public static function setMaxLengthForDynamoDb(): void
    {
        self::$maxLength = 400 * 1024;
    }

    public function forget(...$keys): int
    {
        $removed = 0;
        foreach ($keys as $key) {
            $key = $this->ensureKey($key);
            if ($this->getCache()->forget($key)) {
                $removed++;
            }
        }

        return $removed;
    }

    public function forgetAuto(): int
    {
        return $this->forget(...$this->getCacheOptions()->forgetAuto);
    }

    public function getCache(): CacheManager
    {
        return self::$cache;
    }

    public function getCacheOptions(): CacheOptions
    {
        return $this->cacheOptions;
    }

    public function setCacheOptions(CacheOptions $cacheOptions): void
    {
        $this->cacheOptions = $cacheOptions;
    }

    public function remember(
        array|string $key,
        \DateInterval|\DateTimeInterface|int|null $ttl,
        \Closure $callback
    ): mixed {
        $key = $this->ensureKey($key);
        if ($this->getCache()->has($key)) {
            return $this->getCache()->get($key);
        }
        $result = $callback();
        if ($this->shouldCache($key, $result)) {
            $this->getCache()->put($key, $result, $ttl);
        }

        return $result;
    }

    protected function ensureKey(string|array $key, ?string $baseName = null): string
    {
        if (is_string($key)) {
            return $key;
        }

        return $this->getCacheOptions()->getNaming($baseName)->get($key[0] ?? null, $key[1] ?? null);
    }

    protected function shouldCache(string $key, mixed $value): bool
    {
        if (self::$maxLength === 0) {
            return true;
        }
        try {
            $serialized = serialize($value);

            return strlen($key) + strlen($serialized) < self::$maxLength;
        } catch (\Throwable) {
            return false;
        }
    }
}
