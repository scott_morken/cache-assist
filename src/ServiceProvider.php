<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 8:40 AM
 */

namespace Smorken\CacheAssist;

/**
 * Class ServiceProvider
 *
 *
 * @codeCoverageIgnore
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        CacheAssist::setCache($this->app['cache']);
        CacheAssist::setMaxLengthForDynamoDb();
    }

    public function register(): void {}
}
