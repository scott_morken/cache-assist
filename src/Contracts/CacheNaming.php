<?php

namespace Smorken\CacheAssist\Contracts;

interface CacheNaming
{
    /**
     * One or both of coreName/identifier must be set
     */
    public function get(?string $coreName = null, ?string $identifier = null): string;

    /**
     * Returns the converted baseName
     */
    public function getBaseName(): string;

    /**
     * Converts and checks to see if the baseName matches the class' baseName
     */
    public function isBaseName(string $baseName): bool;

    /**
     * Converts and stores the baseName
     */
    public function setBaseName(string $baseName): void;
}
