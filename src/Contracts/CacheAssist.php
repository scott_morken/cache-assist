<?php

namespace Smorken\CacheAssist\Contracts;

use Illuminate\Cache\CacheManager;

/**
 * Interface CacheAssist
 */
interface CacheAssist
{
    /**
     * Remove item/items from the cache.
     *
     * @param  string|array  $keys
     */
    public function forget(...$keys): int;

    public function forgetAuto(): int;

    public function getCache(): CacheManager;

    public function getCacheOptions(): CacheOptions;

    /**
     * Get an item from the cache, or execute the given Closure and store the result.
     */
    public function remember(
        array|string $key,
        \DateInterval|\DateTimeInterface|int|null $ttl,
        \Closure $callback
    ): mixed;

    public function setCacheOptions(CacheOptions $cacheOptions): void;

    /**
     * Setup mock of repository.
     */
    public static function mock(): void;

    public static function setCache(CacheManager $cache): void;

    public static function setMaxLengthForDynamoDb(): void;
}
