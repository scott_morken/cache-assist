<?php

namespace Smorken\CacheAssist\Contracts;

/**
 * Interface CacheOptions
 *
 *
 * @property array $forgetAuto Array of [[coreName = null, identifier = null],...]
 * @property \Carbon\Carbon $defaultCacheTime
 * @property \Smorken\CacheAssist\Contracts\CacheNaming[] $cacheNamings
 * @property string|null $baseName
 *
 * @phpstan-require-extends \Smorken\CacheAssist\CacheOptions
 */
interface CacheOptions
{
    /**
     * Returns the CacheNaming object by baseName (or the first if baseName is null)
     */
    public function getNaming(?string $baseName = null): CacheNaming;

    public function getOption(string $key): mixed;

    public function setOption(string $key, mixed $value): void;

    public function setOptions(array $options = []): void;

    public function toArray(): array;
}
