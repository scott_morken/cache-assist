<?php

namespace Smorken\CacheAssist;

class CacheNaming implements \Smorken\CacheAssist\Contracts\CacheNaming
{
    protected string $baseName;

    /**
     * CacheNaming constructor.
     *
     * @param  string  $baseName  Interface/Class name
     */
    public function __construct(string $baseName, protected string $separator = '/')
    {
        $this->separator = $separator;
        $this->setBaseName($baseName);
    }

    public function get(?string $coreName = null, ?string $identifier = null): string
    {
        if ($this->emptyValue($coreName) && $this->emptyValue($identifier)) {
            throw new Exception('At least one of coreName or identifier must be set.');
        }
        $parts = [$this->getBaseName()];
        if ($coreName) {
            $parts[] = $this->convert($coreName);
        }
        if ($identifier) {
            $parts[] = $this->convert($identifier);
        }

        return implode($this->separator, $parts);
    }

    protected function emptyValue(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value);
    }

    public function getBaseName(): string
    {
        return $this->baseName;
    }

    /**
     * @param  string  $baseName  Interface/Class name
     */
    public function setBaseName(string $baseName): void
    {
        $this->baseName = $this->convert($baseName);
    }

    public function isBaseName(string $baseName): bool
    {
        return $this->convert($baseName) === $this->baseName;
    }

    protected function convert(string $baseName): string
    {
        return preg_replace('/[\W]/', '', $baseName);
    }
}
