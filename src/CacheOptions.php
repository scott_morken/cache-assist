<?php

namespace Smorken\CacheAssist;

use Carbon\Carbon;
use Smorken\CacheAssist\Contracts\CacheNaming;

class CacheOptions implements \Smorken\CacheAssist\Contracts\CacheOptions
{
    protected array $options = [
        'forgetAuto' => [],
        'defaultCacheTime' => null,
        'cacheNamings' => [],
        'baseName' => null,
    ];

    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    public function __get(string $key)
    {
        return $this->getOption($key);
    }

    public function __set(string $key, $value): void
    {
        $this->setOption($key, $value);
    }

    public function getNaming(?string $baseName = null): CacheNaming
    {
        $this->setupNaming();
        if ($baseName === null) {
            return $this->cacheNamings[array_key_first($this->cacheNamings)];
        }
        $naming = $this->cacheNamings[$baseName] ?? null;
        if ($naming) {
            return $naming;
        }

        return $this->tryIterateCacheNamingsForBaseName($baseName);
    }

    public function getOption(string $key): mixed
    {
        $this->ensureKeyExists($key);

        return $this->options[$key];
    }

    public function setOption(string $key, mixed $value): void
    {
        $this->ensureKeyExists($key);
        $this->options[$key] = $value;
    }

    public function setOptions(array $options = []): void
    {
        $options = array_replace($this->getDefaultOptions(), $options);
        foreach ($options as $key => $value) {
            $this->setOption($key, $value);
        }
        $this->setupNaming();
    }

    public function toArray(): array
    {
        return $this->options;
    }

    protected function ensureKeyExists(string $key): void
    {
        if (! array_key_exists($key, $this->options)) {
            throw new Exception("Option $key does not exist.");
        }
    }

    protected function getDefaultOptions(): array
    {
        return [
            'defaultCacheTime' => Carbon::now()->addMinutes(10),
        ];
    }

    protected function setupNaming(): void
    {
        if (count($this->cacheNamings) === 0) {
            if (! $this->baseName) {
                throw new Exception('CacheNamings OR baseName option must be set.');
            }
            $this->options['cacheNamings'][$this->baseName] = new \Smorken\CacheAssist\CacheNaming($this->baseName);
        }
    }

    protected function tryIterateCacheNamingsForBaseName(string $baseName): CacheNaming
    {
        foreach ($this->cacheNamings as $cacheNaming) {
            if ($cacheNaming->isBaseName($baseName)) {
                return $cacheNaming;
            }
        }
        throw new Exception("CacheNaming $baseName does not exist.");
    }
}
