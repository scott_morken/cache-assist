<?php

namespace Tests\Smorken\CacheAssist\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\CacheAssist\CacheNaming;
use Smorken\CacheAssist\CacheOptions;
use Smorken\CacheAssist\Exception;

class CacheOptionsTest extends TestCase
{
    public function testGetNamingDefaultValue(): void
    {
        $sut = new CacheOptions(['cacheNamings' => [new CacheNaming('fizbuz'), new CacheNaming('foobar')]]);
        $this->assertEquals('fizbuz', $sut->getNaming()->getBaseName());
    }

    public function testGetNamingWithBaseName(): void
    {
        $sut = new CacheOptions(['baseName' => 'foobar']);
        $this->assertEquals('foobar', $sut->getNaming('foobar')->getBaseName());
    }

    public function testGetNamingWithKeyedArray(): void
    {
        $sut = new CacheOptions(
            ['cacheNamings' => ['fizbuz' => new CacheNaming('fizbuz'), 'foobar' => new CacheNaming('foobar')]]
        );
        $this->assertEquals('foobar', $sut->getNaming('foobar')->getBaseName());
    }

    public function testGetNamingWithNonExistentBaseNameUsingArrayIsException(): void
    {
        $sut = new CacheOptions(['cacheNamings' => [new CacheNaming('foobar')]]);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('CacheNaming fizbuz does not exist.');
        $sut->getNaming('fizbuz');
    }

    public function testGetNamingWithNonExistentBaseNameUsingBaseNameIsException(): void
    {
        $sut = new CacheOptions(['baseName' => 'foobar']);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('CacheNaming fizbuz does not exist.');
        $sut->getNaming('fizbuz');
    }

    public function testGetNamingWithoutKeyedArray(): void
    {
        $sut = new CacheOptions(['cacheNamings' => [new CacheNaming('fizbuz'), new CacheNaming('foobar')]]);
        $this->assertEquals('foobar', $sut->getNaming('foobar')->getBaseName());
    }

    public function testGetOptionNonExistentKeyIsException(): void
    {
        $sut = new CacheOptions(['baseName' => 'foobar']);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Option foo does not exist.');
        $sut->getOption('foo');
    }

    public function testGetOptionWithValidKey(): void
    {
        $sut = new CacheOptions(['baseName' => 'foobar', 'forgetAuto' => [['foo', 'bar']]]);
        $this->assertEquals([['foo', 'bar']], $sut->forgetAuto);
    }

    public function testInstantiateWithNonExistentKeyIsException(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Option foo does not exist.');
        $sut = new CacheOptions(['foo' => 'bar']);
    }

    public function testInstantiateWithoutCacheNamingsOrBaseNameIsException(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('CacheNamings OR baseName option must be set.');
        $sut = new CacheOptions([]);
    }
}
