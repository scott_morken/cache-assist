<?php

namespace Tests\Smorken\CacheAssist\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\CacheAssist\CacheNaming;
use Smorken\CacheAssist\Exception;

class CacheNamingTest extends TestCase
{
    public function testConvertsBaseName(): void
    {
        $naming = new CacheNaming(static::class);
        $this->assertEquals('TestsSmorkenCacheAssistUnitCacheNamingTest', $naming->getBaseName());
    }

    public function testGetWithCoreName(): void
    {
        $naming = new CacheNaming(static::class);
        $this->assertEquals(
            'TestsSmorkenCacheAssistUnitCacheNamingTest/testGetWithCoreName',
            $naming->get(__FUNCTION__)
        );
    }

    public function testGetWithCoreNameAndIdentifier(): void
    {
        $naming = new CacheNaming(static::class);
        $this->assertEquals(
            'TestsSmorkenCacheAssistUnitCacheNamingTest/testGetWithCoreNameAndIdentifier/12345',
            $naming->get(__FUNCTION__, '12345')
        );
    }

    public function testGetWithCoreNameAndIdentifierAndSeparator(): void
    {
        $naming = new CacheNaming(static::class, '.');
        $this->assertEquals(
            'TestsSmorkenCacheAssistUnitCacheNamingTest.testGetWithCoreNameAndIdentifierAndSeparator.12345',
            $naming->get(__FUNCTION__, '12345')
        );
    }

    public function testGetWithCoreNameAndSeparator(): void
    {
        $naming = new CacheNaming(static::class, '.');
        $this->assertEquals(
            'TestsSmorkenCacheAssistUnitCacheNamingTest.testGetWithCoreNameAndSeparator',
            $naming->get(__FUNCTION__)
        );
    }

    public function testGetWithIdentifier(): void
    {
        $naming = new CacheNaming(static::class);
        $this->assertEquals('TestsSmorkenCacheAssistUnitCacheNamingTest/12345', $naming->get(null, '12345'));
    }

    public function testGetWithIdentifierAndSeparator(): void
    {
        $naming = new CacheNaming(static::class, '.');
        $this->assertEquals('TestsSmorkenCacheAssistUnitCacheNamingTest.12345', $naming->get(null, '12345'));
    }

    public function testGetWithNoCoreNameAndNoIdentifierIsException(): void
    {
        $naming = new CacheNaming(static::class);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('At least one of coreName or identifier must be set.');
        $naming->get();
    }

    public function testIsBaseNameDoesNotMatchDifferentClass(): void
    {
        $naming = new CacheNaming(static::class);
        $this->assertFalse($naming->isBaseName(get_class($naming)));
    }

    public function testIsBaseNameMatchesSameClass(): void
    {
        $naming = new CacheNaming(static::class);
        $this->assertTrue($naming->isBaseName(static::class));
    }
}
