<?php

namespace Tests\Smorken\CacheAssist\Unit;

use Carbon\Carbon;
use Illuminate\Cache\CacheManager;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Contracts\Container\Container;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\CacheAssist\CacheAssist;
use Smorken\CacheAssist\Contracts\CacheOptions;

class CacheAssistTest extends TestCase
{
    public function testForgetAutoWithAll(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions([
            'baseName' => static::class, 'forgetAuto' => [['all']],
        ]));
        $sut->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenCacheAssistUnitCacheAssistTest/all'
        )->andReturn(false);
        $this->assertEquals(0, $sut->forgetAuto());
    }

    public function testForgetAutoWithDefaultIsEmpty(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $sut->getCache()->shouldReceive('forget')->never();
        $this->assertEquals(0, $sut->forgetAuto());
    }

    public function testForgetAutoWithMultipleKeys(): void
    {
        $sut = $this->getSut(
            new \Smorken\CacheAssist\CacheOptions(
                [
                    'baseName' => static::class,
                    'forgetAuto' => [
                        'foo',
                        [__FUNCTION__, '12345'],
                        [null, 'abc'],
                    ],
                ]
            )
        );
        $sut->getCache()->shouldReceive('forget')->once()->with('foo')->andReturn(true);
        $sut->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenCacheAssistUnitCacheAssistTest/testForgetAutoWithMultipleKeys/12345'
        )->andReturn(false);
        $sut->getCache()->shouldReceive('forget')
            ->once()
            ->with('TestsSmorkenCacheAssistUnitCacheAssistTest/abc')
            ->andReturn(true);
        $this->assertEquals(2, $sut->forgetAuto());
    }

    public function testForgetWithMultipleKeys(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $sut->getCache()->shouldReceive('forget')->once()->with('foo')->andReturn(true);
        $sut->getCache()->shouldReceive('forget')->once()->with(
            'TestsSmorkenCacheAssistUnitCacheAssistTest/testForgetWithMultipleKeys/12345'
        )->andReturn(false);
        $sut->getCache()->shouldReceive('forget')
            ->once()
            ->with('TestsSmorkenCacheAssistUnitCacheAssistTest/abc')
            ->andReturn(true);
        $this->assertEquals(2, $sut->forget('foo', [__FUNCTION__, '12345'], [null, 'abc']));
    }

    public function testForgetWithNoKeys(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $this->assertEquals(0, $sut->forget());
    }

    public function testHasAndGetWithCacheNamingKey(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $sut->getCache()->store()->shouldReceive('has')
            ->once()
            ->with('TestsSmorkenCacheAssistUnitCacheAssistTest/testHasAndGetWithCacheNamingKey/12345')
            ->andReturn(true);
        $sut->getCache()->shouldReceive('get')
            ->once()
            ->with('TestsSmorkenCacheAssistUnitCacheAssistTest/testHasAndGetWithCacheNamingKey/12345')
            ->andReturn('result');
        $this->assertEquals(
            'result',
            $sut->remember(
                [__FUNCTION__, '12345'],
                $sut->getCacheOptions()->defaultCacheTime,
                function () {
                    return 'result';
                }
            )
        );
    }

    public function testHasAndGetWithStringKey(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $sut->getCache()->store()->shouldReceive('has')
            ->once()
            ->with('foobar')
            ->andReturn(true);
        $sut->getCache()->shouldReceive('get')
            ->once()
            ->with('foobar')
            ->andReturn('result');
        $this->assertEquals(
            'result',
            $sut->remember(
                'foobar',
                $sut->getCacheOptions()->defaultCacheTime,
                function () {
                    return 'result';
                }
            )
        );
    }

    public function testHasAndPutWithCacheNamingKey(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $sut->getCache()->store()->shouldReceive('has')
            ->once()
            ->with('TestsSmorkenCacheAssistUnitCacheAssistTest/testHasAndPutWithCacheNamingKey/12345')
            ->andReturn(false);
        $sut->getCache()->shouldReceive('get')
            ->never();
        $sut->getCache()->shouldReceive('put')
            ->once()
            ->with('TestsSmorkenCacheAssistUnitCacheAssistTest/testHasAndPutWithCacheNamingKey/12345', 'result',
                m::type(Carbon::class))
            ->andReturn(true);
        $this->assertEquals(
            'result',
            $sut->remember(
                [__FUNCTION__, '12345'],
                $sut->getCacheOptions()->defaultCacheTime,
                function () {
                    return 'result';
                }
            )
        );
    }

    public function testHasAndPutWithCacheNamingKeyValueTooLongToCache(): void
    {
        CacheAssist::$maxLength = 90;
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $sut->getCache()->store()->shouldReceive('has')
            ->once()
            ->with('TestsSmorkenCacheAssistUnitCacheAssistTest/testHasAndPutWithCacheNamingKeyValueTooLongToCache/12345')
            ->andReturn(false);
        $sut->getCache()->shouldReceive('get')
            ->never();
        $sut->getCache()->shouldReceive('put')
            ->never();
        $value = str_repeat('a', 100);
        $this->assertEquals(
            $value,
            $sut->remember(
                [__FUNCTION__, '12345'],
                $sut->getCacheOptions()->defaultCacheTime,
                function () use ($value) {
                    return $value;
                }
            )
        );
    }

    public function testHasAndPutWithStringKey(): void
    {
        $sut = $this->getSut(new \Smorken\CacheAssist\CacheOptions(['baseName' => static::class]));
        $sut->getCache()->store()->shouldReceive('has')
            ->once()
            ->with('foobar')
            ->andReturn(false);
        $sut->getCache()->shouldReceive('get')
            ->never();
        $sut->getCache()->shouldReceive('put')
            ->once()
            ->with('foobar', 'result', m::type(Carbon::class))
            ->andReturn(true);
        $this->assertEquals(
            'result',
            $sut->remember(
                'foobar',
                $sut->getCacheOptions()->defaultCacheTime,
                function () {
                    return 'result';
                }
            )
        );
    }

    public function testStaticSetCache(): void
    {
        $cache = m::mock(CacheManager::class);
        CacheAssist::setCache($cache);
        $this->assertSame(
            $cache,
            (new CacheAssist(new \Smorken\CacheAssist\CacheOptions(['baseName' => 'foo'])))->getCache()
        );
    }

    protected function getApp(array $config): Container
    {
        $app = new \Illuminate\Container\Container();
        $app->singleton('config', fn () => new Repository($config));

        return $app;
    }

    protected function getCacheManager(array $config): CacheManager
    {
        $cache = new CacheManager($this->getApp($config));
        $driver = m::mock(Store::class);
        $cache->extend('test', fn () => $driver);

        return $cache;
    }

    protected function getSut(
        CacheOptions $cacheOptions,
        array $config = []
    ): \Smorken\CacheAssist\Contracts\CacheAssist {
        if (empty($config)) {
            $config = [
                'cache' => [
                    'default' => 'test',
                    'stores' => [
                        'test' => [
                            'driver' => 'test',
                        ],
                    ],
                ],
            ];
        }
        CacheAssist::setCache($this->getCacheManager($config));

        return new CacheAssist($cacheOptions);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        CacheAssist::$maxLength = 0;
    }
}
